<?php

/*
class Score extends Model{
	
	public function __construct(){
		parent::__construct();
	}

}
*/

namespace pongsit\model;

class model{
	
	public $db;
	public $table;
	
	public function __construct(){
	    $this->db = $GLOBALS['db'];
	    $table_name = get_class($this);
	    $classnamespace = strtolower($table_name);
	    $position = strrpos($classnamespace, '\\');
	    if($position > 0){
	    	$this->table = substr($classnamespace, strrpos($classnamespace, '\\') + 1);
	 	}else{
	 		$this->table = $table_name;
	 	}
    }
    
    function get_table_column(){
    	return $this->db->get_table_column($this->table);
    }
    
	function get_info($id){
		$query = "SELECT * FROM `".$this->table."` WHERE id = '".$id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}
	
	function insert($inputs){
		return $this->db->insert($this->table,$inputs);
	}
	
	function insert_once($inputs,$unique_key,$unique_value){
		if(empty($unique_key) || empty($unique_value)){ error_log('Error! using insert once'); return 0; }
		$query = "SELECT * FROM `".$this->table."` WHERE $unique_key='$unique_value';";
		$outputs = $this->db->query_array0($query);
		if(empty($outputs)){
			return $this->db->insert($this->table,$inputs);
		}
	}
	
	function update($inputs,$where=""){
		if(empty($where) && !empty($inputs['id'])){
			return $this->db->update($this->table,$inputs,'id='.$inputs['id']);
		}
		if(!empty($where)){
			return $this->db->update($this->table,$inputs,$where);
		}
	}
	function delete($id){
		return $this->db->delete($this->table,'id='.$id);
	}
	
	function get_last_inserted_id(){
		return $this->db->lastInsertRowID();
	}
	
	function get_all($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT * FROM `".$this->table."` ORDER BY $order_by $sort $limit $offset;";
		//error_log($query);
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function get_all_active($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT * FROM `".$this->table."` where active=1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function get_id($name){
		$outputs = $this->db->query_array0("select id from `".$this->table."` where name='$name';");
		return $outputs['id'];
	}
	
	function get_name($id){
		$outputs = $this->db->query_array0("select name from `".$this->table."` where id='$id';");
		return $outputs['name'];
	}
	
	function quick_search($q){
		if(empty($q)){ return; }
		return $this->db->query_array("select * from `".$this->table."` where name like '%$q%'");
	}
	
	function insert_value($inputs){
		$this->db->insert($this->table.'_value',$inputs);
	}
	
	function get_value_filter($column_id,$filter_id){
		if(!empty($filter_id)){
			$results = $this->db->query_array("select * from `".$this->table."_value` where ".$this->table."_column_id=$column_id and filter_id=$filter_id;");
			return $results;
		}
	}
	
	function insert_value_once($inputs){
		if(empty($this->get_value_filter($inputs[$this->table.'_column_id'],$inputs['filter_id']))){
			$this->db->insert($this->table.'_value',$inputs);
		}
	}
	
	// for dynamic column model
	function insert_column($inputs){
		return $this->db->insert($this->table.'_column',$inputs);
	}
	
	function update_column($inputs){
		return $this->db->update($this->table.'_column',$inputs,'id='.$inputs['id']);
	}
	
	function delete_value($id){
		return $this->db->delete($this->table.'_value','id='.$id);
	}
	
	function get_all_filter_id($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit=-1;
		if(!empty($inputs['limit'])){ $limit=$inputs['limit']; }
		$offset=0;
		if(!empty($inputs['offset'])){ $offset=$inputs['offset']; }
		$query = "SELECT filter_id FROM `".$this->table."_value` GROUP BY filter_id $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function get_column_id($name){
		$outputs = $this->db->query_array0("select id from `".$this->table."_column` where name='$name';");
		return $outputs['id'];
	}
	
	function get_column_info($id){
		// Array ( [id] => 1 [name] => first_name [name_show] => ชื่อ [weight] => [multiple] => 0 [json] => [active] => 1 ) 
		$query = "SELECT * FROM `".$this->table."_column` WHERE id = '".$id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}
	
	function get_column_info_by_name($name){
		// Array ( [id] => 1 [name] => first_name [name_show] => ชื่อ [weight] => [multiple] => 0 [json] => [active] => 1 ) 
		$column_id = $this->get_column_id($name);
		$query = "SELECT * FROM `".$this->table."_column` WHERE id = '".$column_id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}
	
	function get_column_value($column_id,$filter_id){
		if(!empty($column_id) && !empty($filter_id)){
			$query = "SELECT * FROM `".$this->table."_value` WHERE `".$this->table."_column_id` = '".$column_id."' and filter_id = '".$filter_id."' ;";
			$outputs = $this->db->query_array0($query);
			if(!empty($outputs)){
				return $outputs;
			}
		}
	}
	
	function get_column_name($id){
		$query = "SELECT name FROM `".$this->table."_column` WHERE id = '".$id."';";
		$outputs = $this->db->query_array0($query);
		return $outputs['name'];
	}
	function get_column_name_show($id){
		$query = "SELECT name_show FROM `".$this->table."_column` WHERE id = '".$id."';";
		$outputs = $this->db->query_array0($query);
		return $outputs['name_show'];
	}
	
	function get_all_column($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT * FROM `".$this->table."_column` ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function get_all_column_active($inputs=array()){
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT * FROM `".$this->table."_column` where active=1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function update_value($inputs){
		$id = $inputs['id'];
		unset($inputs['id']);
		$this->db->update($this->table.'_value',$inputs,'id='.$id);
	}
	function does_id_exist($id){
		$results = $this->db->query_array0("select count(id) as count from ".$this->table."_column_value where id=$id;");
		return $results['count'];
	}
	function get_value_info($filter_id){
		if(empty($filter_id)){return;}
		$results = $this->db->query_array("select * from `".$this->table."_value` inner join `".$this->table."_column` on 
											".$this->table."_column.id=".$this->table."_value.".$this->table."_column_id 
											where filter_id=$filter_id;");
		return $results;
	}
	
	function get_the_info_by_filter_id($filter_id){
		if(empty($filter_id)){return;}
		$infos = $this->db->query_array("select ".$this->table."_column.name,".$this->table."_value.value  from ".$this->table."_value inner join ".$this->table."_column on 
											".$this->table."_column.id=".$this->table."_value.".$this->table."_column_id 
											where filter_id=$filter_id;");
											
		/*
		error_log(print_r($infos,true));
		(
			[0] => Array
				(
					[value] => 0960599488
					[name] => phone
				)

		)
		*/
		foreach($infos as $_values){
			$results[$_values['name']] = $_values['value'];
		}
		return $results;
	}
	
	function get_the_info($id){
		return $this->get_infos($id);
	}
	
	function get_infos($id){
		$alls = $this->get_all_column_active(array('order_by'=>'weight','sort'=>'asc'));
		/*
		print_r($alls);
		Array ( [1] => Array ( [id] => 2 [name] => first_name [name_show] => ชื่อจริง [weight] => 10 [multiple] => 0 [json] => [active] => 1 ) )
		*/
		$last_id=-1;
		$variables['id'] = $id;
		if(!empty($alls)){
			foreach($alls as $values){
				$the_values = $this->get_value_filter($values['id'],$id);
				$multiple_count=1;
				$multiple_show='';
				foreach($the_values as $_values){
					if($last_id==$values['id']){$multiple_count++;}
					if($multiple_count>1){$multiple_show=$multiple_count;}
					$variables[$values['name']]=$_values['value'];
					$last_id = $values['id'];
				}
			}
		}
		/*
		print_r($variables);
		Array ( [title] => นรต. [first_name] => ตฤน [last_name] => คำพาที [id] => 5 ) 
		*/
		return $variables;
	}
	
	function transfer_to_column_value($filter_id_table){
		// get all filter_id 
		$results = $this->db->query("select * from ".$filter_id_table.";");
		while($row = $results->fetchArray()){
			$infos = $this->get_info($row['id']);
			foreach($infos as $_key=>$_value){
				$_column_id = $this->get_column_id($_key);
				if(!empty($_column_id) && !empty($_value)){
					$this->insert_value_once(array($this->table.'_column_id'=>$_column_id, 'value'=>$_value, 'filter_id'=>$row['id']));
				}
			}
		}
	}
	
	function get_filter_id($value){
		$results = $this->db->query_array0("select * from `".$this->table."_value` where value=$value;");
		return $results['filter_id'];
	}
	
	function count_active(){
		$results = $this->db->query_array0("select count(distinct id) as count from `".$this->table."` where active=1;");
		return $results['count'];
	}
	
	function count_all(){
		$results = $this->db->query_array0("select count(distinct id) as count from `".$this->table."`;");
		return $results['count'];
	}
	
	function inner_sort_by_weight($a, $b) {
    	return $a['weight'] - $b['weight'];
	}
	
	function sort_by_weight($arrays) {
		usort($arrays, array($this ,'inner_sort_by_weight'));
		return $arrays;
	}

}